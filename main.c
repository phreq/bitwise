#include <stdlib.h>
#include "bitwise.h"

int main(int argc, char *argv[]) {
	int number = atoi(argv[1]);
	int bits = atoi(argv[2]);

	printf("%d:\t\t", number);
	print_bits(number);

	printf("\n\n");

	printf("%d >> %d:\t", number, bits);
	print_bits(number >> bits);
	printf("\t%d\n", number >> bits);

	//	printf("\n");

	printf("%d << %d:\t", number, bits);
	print_bits(number << bits);
	printf("\t%d\n", number << bits);

	//	printf("\n");

	printf("%d & %d:\t", number, bits);	
	print_bits(number & bits);
	printf("\t%d\n", number & bits);

	printf("%d | %d:\t", number, bits);
	print_bits(number | bits);
	printf("\t%d\n", number & bits);	

	printf("%d ^ %d:\t", number, bits);
	print_bits(number ^ bits);
	printf("\t%d\n", number ^ bits);

	return 0;
}
