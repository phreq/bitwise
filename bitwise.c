#include "bitwise.h"

void print_bits(uint32_t number) {
	int binary[NUMBER_BITS] = {0};
	uint32_t quotient;
	int i = 0, space_counter = 1;
	for (quotient = number; quotient >= 1; i++) {
		binary[i] = quotient%2;
		quotient/=2;
	}

	for (i = NUMBER_BITS-1; i >= 0; i--) {
		printf("%d", binary[i]);

		if (space_counter < 4) {
			space_counter++;
		}   
		else {
			printf(" ");
			space_counter = 1;
		}
	}
}
